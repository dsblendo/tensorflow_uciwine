
# coding: utf-8

# ## Import wine dataset

# In[19]:


import pandas as pd
import numpy as np
import tensorflow as tf


wine_names = ['Class', 'Alcohol', 'Malic acid', 'Ash', 'Alcalinity of ash', 'Magnesium', 'Total phenols', 'Flavanoids', 'Nonflavanoid phenols', 'Proanthocyanins', 'Color intensity', 'Hue', 'OD280/OD315', 'Proline']
wine_data = pd.read_csv('wine.data', names = wine_names) 
wine_df = pd.DataFrame(wine_data)
wine_df_x = wine_df.loc[:,'Alcohol':'Proline']
wine_df_y = wine_df.loc[:,'Class']     


# ## Create NN
# Create Blank NN (Text: what is neural net, Tensorflow basics and workflow)

# In[9]:


from sklearn.model_selection import train_test_split

def convertClass(val):
    if val == 1:
        return [1, 0, 0]
    elif val == 2:
        return [0, 1, 0]
    else: 
        return [0, 0, 1]
    
Y = wine_df.loc[:,'Class'].values
Y = np.array([convertClass(i) for i in Y])
X = wine_df.loc[:,'Alcohol':'Proline'].values

train_x, test_x, train_y, test_y = train_test_split(X,Y , test_size=0.3, random_state=0)
train_x = train_x.transpose()
train_y = train_y.transpose()
test_x = test_x.transpose()
test_y = test_y.transpose()


# In[22]:



def init_parameters(num_input_features):

    num_hidden_layer =  50 
    num_hidden_layer_2 = 13
    num_output_layer_1 = 3
    
    tf.set_random_seed(1)           
    W1 = tf.get_variable("W1", [num_hidden_layer, num_input_features], initializer = tf.contrib.layers.xavier_initializer(seed=1))
    b1 = tf.get_variable("b1", [num_hidden_layer, 1], initializer = tf.zeros_initializer())
    W2 = tf.get_variable("W2", [num_hidden_layer_2, num_hidden_layer], initializer = tf.contrib.layers.xavier_initializer(seed=1))
    b2 = tf.get_variable("b2", [num_hidden_layer_2, 1], initializer = tf.zeros_initializer())
    W3 = tf.get_variable("W3", [num_output_layer_1, num_hidden_layer_2], initializer = tf.contrib.layers.xavier_initializer(seed=1))
    b3 = tf.get_variable("b3", [num_output_layer_1, 1], initializer = tf.zeros_initializer())
    
    parameters = {"W1": W1,
                  "b1": b1,
                  "W2": W2,
                  "b2": b2,
                  "W3": W3,
                  "b3": b3}
    
    return parameters

def for_prop(X, parameters):

    W1 = parameters['W1']
    b1 = parameters['b1']
    W2 = parameters['W2']
    b2 = parameters['b2']
    W3 = parameters['W3']
    b3 = parameters['b3']
          
    Z1 = tf.add(tf.matmul(W1, X), b1)                     
    A1 = tf.nn.relu(Z1)                                    
    Z2 = tf.add(tf.matmul(W2, A1), b2)                     
    A2 = tf.nn.relu(Z2)                                   
    Z3 = tf.add(tf.matmul(W3, A2), b3)                    
    return Z3

def c(Z3, Y):
    logits = tf.transpose(Z3)
    labels = tf.transpose(Y)
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=labels))
    return cost


# In[23]:


def rand_batches(X, Y, batch_size = 128, seed = 0):
    m = X.shape[1]
    batches = []
    np.random.seed(seed)
    
    # Step 1: Shuffle (X, Y)
    permutation = list(np.random.permutation(m))
    shuffled_X = X[:, permutation]
    shuffled_Y = Y[:, permutation].reshape((Y.shape[0],m))

    # Step 2: Partition (shuffled_X, shuffled_Y). Minus the end case.
    num_batches = math.floor(m/batch_size) 
    for k in range(0, num_batches):
        batch_X = shuffled_X[:, k * batch_size : k * batch_size + batch_size]
        batch_Y = shuffled_Y[:, k * batch_size : k * batch_size + batch_size]
        batch = (batch_X, batch_Y)
        batches.append(batch)
    
    # Handling the end case (last mini-batch < mini_batch_size)
    if m % batch_size != 0:
        batch_X = shuffled_X[:, num_batches * batch_size : m]
        batch_Y = shuffled_Y[:, num_batches * batch_size : m]
        batch = (batch_X, batch_Y)
        batches.append(batch)
    
    return batches


# In[31]:


from tensorflow.python.framework import ops
import math
import time

def nn(train_x, train_y, test_x, test_y, learning_rate ,num_epochs, batch_size, print_cost = True):
        ops.reset_default_graph()
        tf.set_random_seed(1) 
        
        (n_x, m) = train_x.shape 
        n_y = train_y.shape[0]
        
        #initialize
        costs = []
        X = tf.placeholder(tf.float32, [n_x, None], name="X")
        Y = tf.placeholder(tf.float32, [n_y, None], name="Y")
        parameters = init_parameters(13)
    
        # Forward propagation: Build the forward propagation in the tensorflow graph
        Z3 = for_prop(X, parameters)
        cost = c(Z3, Y)
        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
        init = tf.global_variables_initializer()
        
        with tf.Session() as sess:
            sess.run(init)
        
            #training loop
            for epoch in range(num_epochs):

                epoch_cost = 0. 
                start = time.time()
                num_batches = int(m / batch_size)
                batches = rand_batches(train_x, train_y, batch_size, 13)

                for batch in batches:
                    (batch_X, batch_Y) = batch
                    _ , batch_cost = sess.run([optimizer, cost], feed_dict={X: batch_X, Y: batch_Y})
                    epoch_cost += batch_cost / num_batches
                    
                # Print the cost every epoch
                if print_cost == True and epoch % 100 == 0:
                    print ("Epoch %i cost: %f" % (epoch, epoch_cost))
                    print ('Time taken for this epoch {} sec\n'.format(time.time() - start))
                if print_cost == True and epoch % 5 == 0:
                    costs.append(epoch_cost)

            # lets save the parameters in a variable
            parameters = sess.run(parameters)
            print("Parameters trained...")

            # Calculate the correct predictions
            correct_prediction = tf.equal(tf.argmax(Z3), tf.argmax(Y))

            # Calculate accuracy on the test set
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))

            print("Train Accuracy:", accuracy.eval({X: train_x, Y: train_y}))
            print("Test Accuracy:", accuracy.eval({X: test_x, Y: test_y}))

            return parameters


# In[32]:


learning_rate = 0.001 #change this to change learning rate
num_epochs = 1000     #change this to change number of epochs
batch_size = 15       #change this to change batch size
parameters = nn(train_x, train_y, test_x, test_y, learning_rate ,num_epochs, batch_size) 

