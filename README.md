# TensorFlow_UCIwine

I build a tensorflow model to correctly predict the Class of each observation in the UCI wine dataset. 


To run in conda environment:

conda create --name py365 python=3.6.5 --channel conda-forge
conda activate py365
conda install pandas
conda install tensorflow
conda install scikit-learn

python3.6 TensorFlow_UCIwine.py